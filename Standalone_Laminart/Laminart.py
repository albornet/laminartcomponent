# ! /usr/bin/env python
# NEST implementation of the LAMINART model of visual cortex.
# Created by Greg Francis (gfrancis@purdue.edu) as part of the Human Brain Project.
import os, sys, numpy, random, nest, matplotlib.pyplot as plt
from images2gif import writeGif
from setInput import *
from createFilters import *
from makeGifs import gifMaker
from multiprocessing import Pool, cpu_count

# Main parameters (the following sections are secondary parameters)
ConditionNames      = ['circles 2', 'hexagons 2'] #, 'octagons 3', 'crosses', 'boxes and crosses', 'squares 1', 'squares 2', 'squares 4', 'octagons 3', 'octagons 2', 'malania short 8']
numTrials           =   1        # number of trials within each condition
resolution          =   0.1      # time step of the computation of the neurons dynamical equations
stimulusTime        = 350.0      # stimulus duration [ms]
numOrientations     =   8	     # number of orientations (2, 4 or 8 ; 8 is experimental)
numSegLayers        =   3        # set the number of segmentation layers ; one of these is the baseline layer (common = 3, min = 1)

# General parameters
inputGain           = 100.0      # how much signal goes to LGN cells ; original = 10.0
inputScale          = 1.0        # general scale for all inputs to the netowrk (LGN, grouping signal, segmentation signal)
weightScale         = 1.0        # general scale for all connections between neurons
useTemplate         = 1          # use template match or not (draw the template on the combo gif, if so)
templateSize        = 18         # size of the template (side length) ; in pixels ; 2*vBarLength-2 usually (18 for vBarLength = 10 pixels)
vSide               = 1          # side of the vernier (1 = bottom bar on the right ; -1 = bottom bar on the left) ; must correspond to actual stimulus
nCoresToUse         = 1          # define here how many cores you want to use (1 for normal for-loop)
nCoresToUse = min(max(1,nCoresToUse),cpu_count()) # make sure that no irrelevant number is chosen

# Oriented filters parameters
oriFilterSize       = 4 # 6      # better as an even number (filtering from LGN to V1) ; 4 is original ; adapt to nOrientations?
V1PoolRange         = 1          # how far activity is pooled in V1
V2PoolRange         = 3          # how far activity is pooled in V2
sigmaX              = 0.75       # how far the filters expand across the filter's orientation
sigmaY              = 0.75       # how far the filters expand along  the filter's orientation
oLambda             = 5
phi                 = 0.0*numpy.pi/2.0  # in radians (general phase for the orientations, if needed)
if numOrientations > 4:          # filters have to be a bit tweaked to contain more than 4 different orientations
	oriFilterSize   = 6
	sigmaX          = 0.5
	sigmaY          = 2.5
	oLambda         = 4

# How time goes
stepDuration        = 50.0       # duration of a time step, for the gif [ms]
interTrialInterval  = 10.0       # number of time steps between each stimulus ; originally 20.0
startSegSignal      = 150.0      # in milliseconds ; originally 150.0
stimulusTimeSteps   = stimulusTime/stepDuration # number of time steps of duration stepDuration
gifSlowMotionRate   = 4.0        # how much slower you see the neurons activity in the gifs ; originally 4.0

# Grouping mechanism parameters
dampingInput 	    =  100.0     # this input will block the boundary spreading control for a certain amount of time [pA]
longInput           =  400.0     # constant input controlling long range spreads
boundaryGroupSpeeds =   [1,5]    # [1,5] ; in pixel(s) per grouping step
groupTrigSegDelay   =   35.0     # time [ms] between the damping delay turn off and the segmentation signal turn on ;

# Filling-in parameters
V4SpreadingRange   = 1                     # V4 cells activity spontaneously spread
V1TriggerRange     = int(oriFilterSize/2)  # V1 polar cells trigger activity in V4, on their polar receptive field size
V2StopRange        = None                  # never used it (V2 apolar cells simply inhibit V4 activity)
numBrightnessFlows = 4
brightSpreadSpeeds = [1]

# Segmentation parameters
useSegmentation     =   1        # set to 0 if you want to have several segmentation layers, but not to send any segmentation signal
segSignalSize       =  10        # on the cortical map
segLocSD            =   0.1      # on the cortical map
segSignalAmplitude  = 100.0      # gain for the segmentation signals ; in pA (?)
segSignalDuration   =  50.0      # in ms
numSegFlows         =   4
boundarySegSpeeds   =  [1]       # FOR NOW: ONLY WORKS WITH [1] ; in pixel(s) per spreading step

# Cortical magnification parameters
useCMF              = 0          # use the cortical magnification factor to model the precision of segmentation signals
eccentricity        = 9.0        # distance (in degrees) between fixation point and Vernier
pixPerDeg           = (20.0/1.4) # 20 pixels for the height of the Vernier (1.4 degrees)
a                   = 0.3*pixPerDeg # fovea "radius" ; 0.3 degrees (?), but in pixels
b                   = 60         # slope of the cortical magnification (could depend on pixPerDeg)

# Neurons parameters (only custom parameters of the neurons are defined here)
cellParams = {} # {'V_th': -56.0,}

# Connection parameters
connections = {

    # Input and LGN
	'InputToLGN'               :    700.0, #   700
	'LGN_ToV1Excite'           :    600.0, #   400
	'LGN_ToV4Excite'           :    280.0, #   280

    # V1 classic
    'V1_SimpleExcite'          :      1.0, #     1
    'V1_SurroundInhib'         :     -1.0, #    -1
    'V1_ComplexExcite'         :    500.0, #   500
    'V1_ComplexInhib'          :   -500.0, #  -500
    'V1_CrossOriInhib'         :  -1000.0, #  1000
    'V1_Feedback'              :    500.0, #   500
    'V1_NegFeedback'           :  -1500.0, # -1500
    'V1_EndCutExcite'          :   1500.0, #  1500
    'V1_InterInhib'            :  -1500.0, # -1500
    'V1_FoldedFeedback'        :    100.0, #   100
    'V1_ToV2Excite'            :  10000.0, # 10000

    # V2 classic
    'V2_SimpleExcite'          :      1.0, #     1
    'V2_SurroundInhib'         :     -2.0, #     2
    'V2_ComplexExcite'         :   2000.0, #  2000
    'V2_CrossOriInhib'         :  -1000.0, # -1000
    'V2_EndCutExcite'          :    500.0, #   500 (not used)
    'V2_SegmentExcite'         :   2000.0, #  2000
    'V2_SegmentInhib'          : -20000.0, # 20000

    # V2 grouping
    'V2_Spreading'             :    750.0, #   750 ; directionnal spreading, active until control inhibition kicks in
	'V2_SpreadingLong'         :    750.0, #   750 ; same, but at a greater range
    'V2_ControlExcite'         :   1000.0, #  1000 ; spreadingly activated interneurons activate spreading control
    'V2_ControlInhib'          :  -2000.0, # -2000 ; how fast inhibition takes effect, once on
    'V2_InputLongExcite'       :     10.0, #    10 ; excitatory input to the inhibitory neurons that control the long-range spreading
	'V2_InputLongInhib'        :  -1000.0, # -1000 ; inhibitory input to the same neurons, only active for a certain delay after simulus onset
    'V2_ControlLongExcite'     :   1000.0, #  1000 ; from V2Layer23 to the long-range spreading layer
    'V2_ControlLongInhib'      :  -1000.0, #  5000 ; from the control layer to the long-range spreading layer
	'V2_ToV1FeedbackExcite'    :    250.0, #   250 ; from V2Layer23 to V1Layer4P1/2, to strengthen illusory contours

	# V4 filling-in
	'V4_BrightnessExcite'      :   5000.0, #  5000
	'V4_DarknessExcite'        :   5000.0, #  5000
	'V4_FlowExcite'            :   1000.0, #  1000
	'V4_FlowInhib'             :  -1000.0, # -1000
	'V2_BoundaryToSurfaceInhib': -50000.0, #-50000

	# Boundary segmentation layers
	'B_SegmentSpreadExcite'    :   1000.0,
	'B_ControlSpreadExcite'    :   3000.0,
	'B_TriggerV2Inhib'         :  -5000.0,
	'B_FeedbackV2Inhib'        : -10000.0,
	'B_FeedbackV1Inhib'        : -10000.0,
	'B_InterNeuronsInhib'      : -15000.0, # -15000
	'B_SendSignalExcite'       :      1.0,
	'B_ResetSignalInhib'       :  -1000.0}

# Scale the weights, if needed
if resolution > 0.1:                               # This condition works for dt = 1.0 ms
	weightScale = 1.1                              # All weights are multiplied by 1.1
	connections['B_ControlSpreadExcite'] = 3700.0  # Some special case
	# connections['V2_ToV1FeedbackExcite'] =  255.0  # Some special case as well
for key, value in connections.items():
	connections[key] = value*weightScale

# Main loop, to run one condition
def runOneStim(thisConditionName):

	#######################################################################################
	### Prepare NEST, read the input, build orientation filters and connection patterns ###
	#######################################################################################

	# Rebuild network for each condition (some vary in image size, and NEST simulations have an upper time limit that might be exceeded)
	nest.sli_run('M_WARNING setverbosity') # avoid writing too much NEST messages
	nest.ResetKernel()
	nest.SetKernelStatus({'resolution': resolution, 'local_num_threads': 4, 'print_time': True})

	# Read the image from Stimuli/ and create boundary coordinates (between pixels coordinates)
	pixels, imageNumPixelRows, imageNumPixelColumns = readBMP(thisConditionName, onlyZerosAndOnes=2)
	numPixelRows    = imageNumPixelRows    + 1                            # Height of the oriented map, in pixels
	numPixelColumns = imageNumPixelColumns + 1                            # Width  of the oriented map, in pixels
	oppOri = list(numpy.roll(range(numOrientations), numOrientations/2))  # Indexes for orthogonal orientations
	sys.stdout.write('\n\n\n*** Condition "' + str(thisConditionName) + '" (' + str(imageNumPixelRows) + 'x' + str(imageNumPixelColumns) + ' pixels). ***\n')

	# Build oriented convolutional filters (LGN to V1), pooling connections filters (inside V1 and V2), grouping filters (inside V2) and segmentation filters
	V1ConvFilters1,   V1ConvFilters2 = createFilters(numOrientations, size=oriFilterSize, phi=phi, sigmaX=sigmaX, sigmaY=sigmaY, oLambda=oLambda, plot=True)
	V1PoolingFilters, V1PoolingConnections1, V1PoolingConnections2 = createPoolingConnectionsAndFilters(numOrientations, VPoolSize=2*V1PoolRange+1, phi=phi)
	brightnessFlowFilter, brightnessBoundaryBlockFilter            = createBrightnessFilters(numOrientations, numBrightnessFlows, brightSpreadSpeeds)
	boundaryGroupFilter   = createGroupingFilters(    numOrientations, boundaryGroupSpeeds, phi)
	boundarySegFlowFilter = createSegmentationFilters(numOrientations, boundarySegSpeeds,   phi)
	dampingDelays         = generateDelays(thisConditionName, pixels, numOrientations)  # Default is [0.0 in every direction]
	startSegSignal        = max(dampingDelays) + groupTrigSegDelay                      # Segmentation signal is triggerd by grouping turn-off

	##################################################################################################
	### Create the neuron layers (LGN, V1, V2, V4, Boundary Segmentation Layers) + Spike Detectors ###
	##################################################################################################

	# LGN
	sys.stdout.write('\nBuilding network: \tLGN,')
	LGNBrightInput         = nest.Create('dc_generator',  imageNumPixelRows*imageNumPixelColumns)
	LGNDarkInput           = nest.Create('dc_generator',  imageNumPixelRows*imageNumPixelColumns)
	LGNBright              = nest.Create('iaf_psc_alpha', imageNumPixelRows*imageNumPixelColumns)
	LGNDark                = nest.Create('iaf_psc_alpha', imageNumPixelRows*imageNumPixelColumns)

	# Area V1
	sys.stdout.write(' V1,')
	V1Layer4P1             = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer4P2             = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer6P1             = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer6P2             = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer23              = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer23Pool          = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer23Inter1        = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)
	V1Layer23Inter2        = nest.Create('iaf_psc_alpha', numOrientations*numPixelRows*numPixelColumns)

	# Area V2
	sys.stdout.write(' V2,')
	V2Layer4               = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer6               = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23              = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23Control       = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23Inhib         = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23Inter1        = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23Inter2        = nest.Create('iaf_psc_alpha',              numOrientations*numPixelRows*numPixelColumns)
	V2Layer23Seg           = nest.Create('iaf_psc_alpha', numSegLayers*numOrientations*numPixelRows*numPixelColumns)
	GroupDampingCell       = nest.Create('dc_generator',               numOrientations)
	if len(boundaryGroupSpeeds) > 1:
		ConstantInputLong  = nest.Create('dc_generator',               numOrientations)
		SpreadControlLong  = nest.Create('iaf_psc_alpha',              numOrientations)
		V2Layer23LongGroup = nest.Create('iaf_psc_alpha', (len(boundaryGroupSpeeds)-1)*numOrientations*numPixelRows*numPixelColumns)

	# Area V4
	sys.stdout.write(' V4')
	V4Brightness           = nest.Create("iaf_psc_alpha", numSegLayers*                                           imageNumPixelRows*imageNumPixelColumns)
	V4BrightnessFlow       = nest.Create("iaf_psc_alpha", numSegLayers*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns)
	V4InterBrightnessOut   = nest.Create("iaf_psc_alpha", numSegLayers*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns)
	V4Darkness             = nest.Create("iaf_psc_alpha", numSegLayers*                                           imageNumPixelRows*imageNumPixelColumns)
	V4DarknessFlow         = nest.Create("iaf_psc_alpha", numSegLayers*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns)
	V4InterDarknessOut     = nest.Create("iaf_psc_alpha", numSegLayers*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns)


	# Boundary segmentation
	if numSegLayers > 1:
		sys.stdout.write(', Segmentation')
		BoundarySegmentationOn        = nest.Create('iaf_psc_alpha', (numSegLayers-1)*numPixelRows*numPixelColumns)
		BoundarySegmentationOnInter3  = nest.Create('iaf_psc_alpha', (numSegLayers-1)*numPixelRows*numPixelColumns)
		BoundarySegmentationOnSignal  = nest.Create('dc_generator',  (numSegLayers-1)*numPixelRows*numPixelColumns)
		ResetBoundarySegmentationCell = nest.Create('dc_generator')

	# Population to record, and make gifs from
	popToRecord = {}
	popToRecord['LGNBright'] = {'ID': LGNBright,    'dimTuple': (1,                        imageNumPixelRows, imageNumPixelColumns), 'orientedMap': False}
	popToRecord['LGNDark']   = {'ID': LGNDark,      'dimTuple': (1,                        imageNumPixelRows, imageNumPixelColumns), 'orientedMap': False}
	popToRecord['V1']        = {'ID': V1Layer23,    'dimTuple': (1,            numOrientations, numPixelRows,      numPixelColumns), 'orientedMap': True }
	popToRecord['V2']        = {'ID': V2Layer23Seg, 'dimTuple': (numSegLayers, numOrientations, numPixelRows,      numPixelColumns), 'orientedMap': True }
	popToRecord['V4Bright']  = {'ID': V4Brightness, 'dimTuple': (numSegLayers,             imageNumPixelRows, imageNumPixelColumns), 'orientedMap': False}
	popToRecord['V4Dark']    = {'ID': V4Darkness,   'dimTuple': (numSegLayers,             imageNumPixelRows, imageNumPixelColumns), 'orientedMap': False}
	if useSegmentation and numSegLayers > 1:
		popToRecord['SegNet'] = {'ID': BoundarySegmentationOn,       'dimTuple': (numSegLayers-1, numPixelRows, numPixelColumns), 'orientedMap': False}
		popToRecord['Inter3'] = {'ID': BoundarySegmentationOnInter3, 'dimTuple': (numSegLayers-1, numPixelRows, numPixelColumns), 'orientedMap': False}


	#######################################################################
	###  Neurons layers are defined, now set up connexions between them ###
	#######################################################################

	############ LGN cells ############
	sys.stdout.write('\nSetting up connections: LGN,')

	nest.Connect(LGNBrightInput, LGNBright, 'one_to_one', {'weight': connections['InputToLGN']})
	nest.Connect(LGNDarkInput,   LGNDark,   'one_to_one', {'weight': connections['InputToLGN']})


	############ Area V1 ############
	sys.stdout.write(' V1,')

	oriFilterWeight = connections['LGN_ToV1Excite']
	for k in range(numOrientations):                             # Orientations
		for i2 in range(-oriFilterSize/2, oriFilterSize/2):      # Filter rows
			for j2 in range(-oriFilterSize/2, oriFilterSize/2):  # Filter columns
				source  = []
				target  = []
				source2 = []
				target2 = []
				for i in range(oriFilterSize/2, numPixelRows-oriFilterSize/2):  # Rows
					for j in range(oriFilterSize/2, numPixelColumns-oriFilterSize/2):  # Columns
						if i+i2 >=0 and i+i2<imageNumPixelRows and j+j2>=0 and j+j2<imageNumPixelColumns:

							# Connections from LGN to oriented polarized V1 cells, using convolutionnal filters (1st polarity)
							if abs(V1ConvFilters1[k][i2+oriFilterSize/2][j2+oriFilterSize/2]) > 0.1:
								source.append(                                (i+i2)*imageNumPixelColumns + (j+j2))
								target.append(k*numPixelRows*numPixelColumns + i    *numPixelColumns      +  j    )

							# Connections from LGN to oriented polarized V1 cells, using convolutionnal filters (2nd polarity)
							if abs(V1ConvFilters2[k][i2+oriFilterSize/2][j2+oriFilterSize/2]) > 0.1:
								source2.append(                                (i+i2)*imageNumPixelColumns + (j+j2))
								target2.append(k*numPixelRows*numPixelColumns + i    *numPixelColumns      +  j    )

				# Define the weight for this filter position and orientation
				polarWeight1 = oriFilterWeight*V1ConvFilters1[k][i2+oriFilterSize/2][j2+oriFilterSize/2]
				polarWeight2 = oriFilterWeight*V1ConvFilters2[k][i2+oriFilterSize/2][j2+oriFilterSize/2]

				# LGN -> Layer 6 (simple cells) connections (no connections at the edges, to avoid edge-effects)
				nest.Connect([LGNBright[s] for s in source],  [V1Layer6P1[t] for t in target],  'one_to_one', {"weight": polarWeight1})
				nest.Connect([LGNDark[s]   for s in source],  [V1Layer6P2[t] for t in target],  'one_to_one', {"weight": polarWeight1})
				nest.Connect([LGNBright[s] for s in source2], [V1Layer6P2[t] for t in target2], 'one_to_one', {"weight": polarWeight2})
				nest.Connect([LGNDark[s]   for s in source2], [V1Layer6P1[t] for t in target2], 'one_to_one', {"weight": polarWeight2})

				# LGN -> Layer 4 (simple cells) connections (no connections at the edges, to avoid edge-effects)
				nest.Connect([LGNBright[s] for s in source],  [V1Layer4P1[t] for t in target],  'one_to_one', {"weight": polarWeight1})
				nest.Connect([LGNDark[s]   for s in source],  [V1Layer4P2[t] for t in target],  'one_to_one', {"weight": polarWeight1})
				nest.Connect([LGNBright[s] for s in source2], [V1Layer4P2[t] for t in target2], 'one_to_one', {"weight": polarWeight2})
				nest.Connect([LGNDark[s]   for s in source2], [V1Layer4P1[t] for t in target2], 'one_to_one', {"weight": polarWeight2})

	# Excitatory connection from same orientation and polarity 1, input from layer 6
	nest.Connect(V1Layer6P1, V1Layer4P1, 'one_to_one', {"weight": connections['V1_SimpleExcite']})
	nest.Connect(V1Layer6P2, V1Layer4P2, 'one_to_one', {"weight": connections['V1_SimpleExcite']})

	# Complex projection to V1Layer23 cells
	nest.Connect(V1Layer4P1, V1Layer23, 'one_to_one', {"weight": connections['V1_ComplexExcite']})
	nest.Connect(V1Layer4P2, V1Layer23, 'one_to_one', {"weight": connections['V1_ComplexExcite']})

	# Connect Layer 23 cells to Layer 6 cells (folded feedback)
	nest.Connect(V1Layer23, V1Layer6P1, 'one_to_one', {"weight": connections['V1_FoldedFeedback']})
	nest.Connect(V1Layer23, V1Layer6P2, 'one_to_one', {"weight": connections['V1_FoldedFeedback']})

	source  = []
	source2 = []
	source3 = []
	source4 = []
	source5 = []
	source6 = []
	source7 = []
	target  = []
	target2 = []
	target3 = []
	target4 = []
	target5 = []
	target6 = []
	target7 = []
	for k in range(numOrientations):          # Orientations
		for i in range(numPixelRows):         # Rows
			for j in range(numPixelColumns):  # Columns

				# Cross orientations inhibition
				for k2 in range(numOrientations):
					if k != k2 : # and k != oppOri[k2]:
						source.append(k *numPixelRows*numPixelColumns + i*numPixelColumns + j)
						target.append(k2*numPixelRows*numPixelColumns + i*numPixelColumns + j)

				# Neighbouring connections
				for i2 in range(-V1PoolRange, V1PoolRange+1):      # Filter rows (extra +1 to insure get top of odd-numbered filter)
					for j2 in range(-V1PoolRange, V1PoolRange+1):  # Filter columns
						if 0<=i+i2<numPixelRows and 0<=j+j2<numPixelColumns:

							# Surround inhibition
							if (i2!=0 or j2!=0): # (Greg small error was like this: and i2!=1 and j2!=1:)
								source2.append(k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
								target2.append(k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )

							# Pooling around
							if V1PoolingFilters[k][i2+V1PoolRange][j2+V1PoolRange] > 0:
								source3.append(       k *numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
								target3.append(       k *numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
								source4.append(oppOri[k]*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
								target4.append(       k *numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )

							# Pooling back, one side
							if V1PoolingConnections1[k][i2+V1PoolRange][j2+V1PoolRange] > 0:
								source5.append(k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
								target5.append(k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

							# Pooling back, other side
							if V1PoolingConnections2[k][i2+V1PoolRange][j2+V1PoolRange] > 0:
								source6.append(k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
								target6.append(k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

				# End-cutting (...)
				source7.append(oppOri[k]*numPixelRows*numPixelColumns + i*numPixelColumns + j)
				target7.append(       k *numPixelRows*numPixelColumns + i*numPixelColumns + j)

	# Cross-orientation inhibition
	nest.Connect([V1Layer23[s]       for s in source ], [V1Layer23[t]       for t in target ], 'one_to_one', {"weight": connections['V1_CrossOriInhib']})

	# Surround inhibition from layer 6 of same orientation and polarity
	nest.Connect([V1Layer6P1[s]      for s in source2], [V1Layer4P1[t]      for t in target2], 'one_to_one', {"weight": connections['V1_SurroundInhib']})
	nest.Connect([V1Layer6P2[s]      for s in source2], [V1Layer4P2[t]      for t in target2], 'one_to_one', {"weight": connections['V1_SurroundInhib']})

	# Pooling neurons in Layer 23 (excitation from same orientation, inhibition from orthogonal)
	nest.Connect([V1Layer23[s]       for s in source3], [V1Layer23Pool[t]   for t in target3], 'one_to_one', {"weight": connections['V1_ComplexExcite']})
	nest.Connect([V1Layer23[s]       for s in source4], [V1Layer23Pool[t]   for t in target4], 'one_to_one', {"weight": connections['V1_ComplexInhib']})

	# Pooling neurons back to Layer 23 and to interneurons
	nest.Connect([V1Layer23Pool[s]   for s in source5], [V1Layer23[t]       for t in target5], 'one_to_one', {"weight": connections['V1_Feedback']})
	nest.Connect([V1Layer23Pool[s]   for s in source5], [V1Layer23Inter1[t] for t in target5], 'one_to_one', {"weight": connections['V1_Feedback']})
	nest.Connect([V1Layer23Pool[s]   for s in source5], [V1Layer23Inter2[t] for t in target5], 'one_to_one', {"weight": connections['V1_NegFeedback']})
	nest.Connect([V1Layer23Pool[s]   for s in source6], [V1Layer23[t]       for t in target6], 'one_to_one', {"weight": connections['V1_Feedback']})
	nest.Connect([V1Layer23Pool[s]   for s in source6], [V1Layer23Inter2[t] for t in target6], 'one_to_one', {"weight": connections['V1_Feedback']})
	nest.Connect([V1Layer23Pool[s]   for s in source6], [V1Layer23Inter1[t] for t in target6], 'one_to_one', {"weight": connections['V1_NegFeedback']})

	# Connect interneurons to complex cell and each other
	nest.Connect( V1Layer23Inter1,                       V1Layer23,                            'one_to_one', {"weight": connections['V1_InterInhib']})
	nest.Connect( V1Layer23Inter2,                       V1Layer23,                            'one_to_one', {"weight": connections['V1_InterInhib']})

	# End-cutting (excitation from orthogonal interneuron)
	nest.Connect([V1Layer23Inter1[s] for s in source7], [V1Layer23[t]       for t in target7], 'one_to_one', {"weight": connections['V1_EndCutExcite']})
	nest.Connect([V1Layer23Inter2[s] for s in source7], [V1Layer23[t]       for t in target7], 'one_to_one', {"weight": connections['V1_EndCutExcite']})


	############ Area V2 ############
	sys.stdout.write(' V2,')

	# V1 projection to V2 and further to layer 4 and layer 23
	nest.Connect(V1Layer23,  V2Layer6,  'one_to_one', {'weight': connections['V1_ToV2Excite'   ]})
	nest.Connect(V1Layer23,  V2Layer4,  'one_to_one', {'weight': connections['V1_ToV2Excite'   ]})
	nest.Connect(V2Layer6,   V2Layer4,  'one_to_one', {'weight': connections['V2_SimpleExcite' ]})
	nest.Connect(V2Layer4,   V2Layer23, 'one_to_one', {'weight': connections['V2_ComplexExcite']})

	source  = []
	source2 = []
	source3 = []
	source4 = []
	target  = []
	target2 = []
	target3 = []
	target4 = []
	for k in range(numOrientations):                  # Orientations
		for i in range(numPixelRows):                 # Rows
			for j in range(numPixelColumns):          # Columns

				# Cross-orientations inhibition
				for k2 in range(0, numOrientations):  # Orientations (target)
					if k != k2 and k != oppOri[k2]:
						source.append(k *numPixelRows*numPixelColumns + i*numPixelColumns + j)
						target.append(k2*numPixelRows*numPixelColumns + i*numPixelColumns + j)

				# Surround inhibition
				for i2 in range(-1, 1+1):
					for j2 in range(-1,1+1):
						if i+i2 >= 0 and i+i2 < numPixelRows and i2 != 0 and j+j2 >= 0 and j+j2 < numPixelColumns and j2 != 0:
							source2.append(k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
							target2.append(k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )

				# Projection from V2Layer23 to the segmentation cells
				for h in range(numSegLayers):
					source3.append(                                                 k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
					target3.append(h*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)

					# Boundary inhibition between segmentation layers
					for h2 in range(h, numSegLayers-1):
							source4.append( h    *numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
							target4.append((h2+1)*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)

	# Cross-orientations inhibition and surround inhibition
	nest.Connect([V2Layer23[s] for s in source ], [V2Layer23[t] for t in target ], 'one_to_one', {'weight': connections['V2_CrossOriInhib']})
	nest.Connect([V2Layer6[s]  for s in source2], [V2Layer4[t]  for t in target2], 'one_to_one', {'weight': connections['V2_SurroundInhib']})

	# Projection from V2Layer23 to the segmentation cells and inhibition between segmentation layers
	nest.Connect([V2Layer23[s]    for s in source3], [V2Layer23Seg[t] for t in target3], 'one_to_one', {'weight': connections['V2_SegmentExcite']})
	nest.Connect([V2Layer23Seg[s] for s in source4], [V2Layer23Seg[t] for t in target4], 'one_to_one', {'weight': connections['V2_SegmentInhib' ]})

	# All the connections computing illusory contours
	for side in range(2):                                    # Grouping either on one side or the other
		sideValue = int((-2)*(side-0.5))                     # Useful sign (1 for side = 0, -1 for side = 1)
		for h in range(len(boundaryGroupSpeeds)):            # Different ranges of spreading
			center = boundaryGroupSpeeds[h]		             # Center of the grouping filter
			for k in range(numOrientations):                 # Source orientations
				for k2 in range(numOrientations):			 # Target orientations
					for i2 in range(-center, center+1):      # Filter rows
						for j2 in range(-center, center+1):  # Filter columns

							# Grouping connections on each side ; the connection weights are taken from the grouping filters
							groupingWeight     = connections['V2_Spreading'    ]*boundaryGroupFilter[side][h][k][k2][i2+center][j2+center]
							groupingWeightLong = connections['V2_SpreadingLong']*boundaryGroupFilter[side][h][k][k2][i2+center][j2+center]
							if groupingWeight > 0:

								source  = []
								target  = []
								source2 = []
								target2 = []
								for i in range(numPixelRows):         # Rows
									for j in range(numPixelColumns):  # Columns
										if i+i2 >= 0 and i+i2 < numPixelRows and j+j2 >= 0 and j+j2 < numPixelColumns:

											# Connections among V2Layer23 cells and bipole circuit
											if h==0:
												target.append(k *numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
												source.append(k2*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

											# Connection from long range grouping cells to V2 cells
											elif len(boundaryGroupSpeeds) > 1:
												source2.append((h-1)*numOrientations*numPixelRows*numPixelColumns + k2*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
												target2.append(                                                     k *numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )

								# V2Layer23 spreads to neighbors and excites or inhibits interneurons on each side
								nest.Connect([V2Layer23[s]     for s in source], [V2Layer23[t]       for t in target], 'one_to_one', {'weight':  groupingWeight          })
								nest.Connect([V2Layer23[s]     for s in source], [V2Layer23Inter1[t] for t in target], 'one_to_one', {'weight':  sideValue*groupingWeight})
								nest.Connect([V2Layer23[s]     for s in source], [V2Layer23Inter2[t] for t in target], 'one_to_one', {'weight': -sideValue*groupingWeight})

								# Grouping at longer ranges
								if len(boundaryGroupSpeeds) > 1:
									nest.Connect([V2Layer23LongGroup[s] for s in source2], [V2Layer23[t] for t in target2], 'one_to_one', {'weight': groupingWeightLong})

	# Grouping interneurons 1 and 2 excite the control layer, in turn controling V2Layer23
	nest.Connect(V2Layer23Inter1,  V2Layer23Control,  'one_to_one', {'weight': connections['V2_ControlExcite']})
	nest.Connect(V2Layer23Inter2,  V2Layer23Control , 'one_to_one', {'weight': connections['V2_ControlExcite']})
	nest.Connect(V2Layer23Control, V2Layer23,         'one_to_one', {'weight': connections['V2_ControlInhib' ]})

	# Damping on control (orientation specific)
	source  = []
	target  = []
	source2 = []
	target2 = []
	source3 = []
	target3 = []
	for k in range(numOrientations):          # Orientations
		for i in range(numPixelRows):         # Rows
			for j in range(numPixelColumns):  # Columns
				source.append(k)
				target.append(k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
				for h in range(len(boundaryGroupSpeeds)-1):
					source2.append(                                                 k                                                     )
					target2.append(h*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
					source3.append(                                                 k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
					target3.append(h*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)

	# These connections are active during the damping delay (orientation specific)
	nest.Connect(    [GroupDampingCell[s]  for s in source ], [V2Layer23Control[t]   for t in target ], 'one_to_one', {'weight': connections['V2_ControlInhib'     ]})
	if len(boundaryGroupSpeeds) > 1:
		nest.Connect( ConstantInputLong,                       SpreadControlLong,                       'one_to_one', {'weight': connections['V2_InputLongExcite'  ]})
		nest.Connect( GroupDampingCell,                        SpreadControlLong,                       'one_to_one', {'weight': connections['V2_InputLongInhib'   ]})
		nest.Connect([SpreadControlLong[s] for s in source2], [V2Layer23LongGroup[t] for t in target2], 'one_to_one', {'weight': connections['V2_ControlLongInhib' ]})
		nest.Connect([V2Layer23[s]         for s in source3], [V2Layer23LongGroup[t] for t in target3], 'one_to_one', {'weight': connections['V2_ControlLongExcite']})

	# # V2 is fed back to V1 (espescially the illusory contours)
	nest.Connect(V2Layer23, V1Layer4P1, 'one_to_one', {'weight': connections['V2_ToV1FeedbackExcite']})
	nest.Connect(V2Layer23, V1Layer4P2, 'one_to_one', {'weight': connections['V2_ToV1FeedbackExcite']})


	############## Area V4 ################
	sys.stdout.write(' V4')

	source  = []
	source2 = []
	source3 = []
	source4 = []
	source5 = []
	target  = []
	target2 = []
	target3 = []
	target4 = []
	target5 = []

	# Surface flow, in every V4 segmentation layer
	for h in range(numSegLayers):                             # Segmentation layers
		for i in range(imageNumPixelRows):                    # Rows
			for j in range(imageNumPixelColumns):             # Columns
				for k in range(numBrightnessFlows):           # Flow directions
					for s in range(len(brightSpreadSpeeds)):  # Flow speeds

						# LGN->V4Flow
						source.append(i*imageNumPixelColumns + j)
						target.append(h*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + s*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + k*imageNumPixelRows*imageNumPixelColumns + i*imageNumPixelColumns + j)

						# Connect V4Flow to V4Brightness/darkness
						source2.append(h*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + s*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + k*imageNumPixelRows*imageNumPixelColumns + i*imageNumPixelColumns + j)
						target2.append(h*imageNumPixelRows*imageNumPixelColumns + i*imageNumPixelColumns + j)

						i2 = brightnessFlowFilter[s][k][0]
						j2 = brightnessFlowFilter[s][k][1]
						if i + i2 >= 0 and i + i2 < imageNumPixelRows and j + j2 >= 0 and j + j2 < imageNumPixelColumns:
							target3.append(h*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + s*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + k*imageNumPixelRows*imageNumPixelColumns + (i+i2)*imageNumPixelColumns + (j+j2))
							source3.append(h*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + s*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + k*imageNumPixelRows*imageNumPixelColumns + i*imageNumPixelColumns + j)

		# Boundary blocking of spreading
		for i in range(numPixelRows):                                                         # Rows
			for j in range(numPixelColumns):                                                  # Columns
				for s in range(len(brightSpreadSpeeds)):                                      # Flow speeds
					for k in range(numBrightnessFlows):                                       # Flow directions
						for k2 in range(len(brightnessBoundaryBlockFilter[s][k])):            # Boundary block for given speed
							for k3 in range(numOrientations):                                 # Blocking orientation
								if brightnessBoundaryBlockFilter[s][k][k2][0] != k3 or 1==1:  # first index in BBF indicates an orientation that does NOT block
									i2 = brightnessBoundaryBlockFilter[s][k][k2][1]
									j2 = brightnessBoundaryBlockFilter[s][k][k2][2]
									if i+i2>=0 and i + i2 < imageNumPixelRows and j+j2>=0 and j + j2 < imageNumPixelColumns:
										source4.append(h*numOrientations*numPixelRows*numPixelColumns + k3*numPixelRows*numPixelColumns + i*numPixelColumns + j)
										target4.append(h*len(brightSpreadSpeeds)*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + s*numBrightnessFlows*imageNumPixelRows*imageNumPixelColumns + k*imageNumPixelRows*imageNumPixelColumns + (i+i2)*imageNumPixelColumns + (j+j2))

	# LGNBright->V4BrightnessFlow and LGNDark->V4DarknessFlow
	nest.Connect([LGNBright[s] for s in source], [V4BrightnessFlow[t] for t in target], 'one_to_one', {'weight': connections['LGN_ToV4Excite']})
	nest.Connect([LGNDark[s]   for s in source], [V4DarknessFlow[t]   for t in target], 'one_to_one', {'weight': connections['LGN_ToV4Excite']})

	# V4Flow<->Interneurons
	nest.Connect(V4BrightnessFlow, V4InterBrightnessOut, 'one_to_one', {'weight': connections['V4_BrightnessExcite']})
	nest.Connect(V4DarknessFlow,   V4InterDarknessOut,   'one_to_one', {'weight': connections['V4_DarknessExcite'  ]})

	# V4Flow ->V4Brightness/Darkness
	nest.Connect([V4BrightnessFlow[s] for s in source2], [V4Brightness[t] for t in target2], 'one_to_one', {'weight': connections['V4_FlowExcite']})
	nest.Connect([V4DarknessFlow[s]   for s in source2], [V4Darkness[t]   for t in target2], 'one_to_one', {'weight': connections['V4_FlowExcite']})
	nest.Connect([V4DarknessFlow[s]   for s in source2], [V4Brightness[t] for t in target2], 'one_to_one', {'weight': connections['V4_FlowInhib' ]})
	nest.Connect([V4BrightnessFlow[s] for s in source2], [V4Darkness[t]   for t in target2], 'one_to_one', {'weight': connections['V4_FlowInhib' ]})

	# V4Brightness neighbors<->Interneurons
	nest.Connect([V4InterBrightnessOut[s] for s in source3], [V4BrightnessFlow[t] for t in target3], 'one_to_one', {'weight': connections['V4_BrightnessExcite']})
	nest.Connect([V4InterDarknessOut[s]   for s in source3], [V4DarknessFlow[t]   for t in target3], 'one_to_one', {'weight': connections['V4_BrightnessExcite']})

	# V2layer23 -> V4 Interneurons (all boundaries block except for orientation of flow)
	nest.Connect([V2Layer23Seg[s] for s in source4], [V4InterBrightnessOut[t] for t in target4], 'one_to_one', {'weight': connections['V2_BoundaryToSurfaceInhib']})
	nest.Connect([V2Layer23Seg[s] for s in source4], [V4InterDarknessOut[t]   for t in target4], 'one_to_one', {'weight': connections['V2_BoundaryToSurfaceInhib']})


	########### Boundary segmentation network ###################
	if numSegLayers > 1:
		sys.stdout.write(', Segmentation')
		sys.stdout.flush()

		source   = []
		source2  = []
		source3  = []
		source4  = []
		source2B = []
		source3B = []
		source4B = []
		target   = []
		target2  = []
		target3  = []
		target4  = []
		target2B = []
		target3B = []
		target4B = []
		for h in range(numSegLayers-1):                      # Segmentation layers (not including baseline layer)
			for i in range(numPixelRows):                    # Rows
				for j in range(numPixelColumns):             # Columns
					for s in range(len(boundarySegSpeeds)):  # TEST

						# All spreading interactions
						for i2 in range(-boundarySegSpeeds[s], boundarySegSpeeds[s]+1):      # [-1,0,1]:
							for j2 in range(-boundarySegSpeeds[s], boundarySegSpeeds[s]+1):  # [-1,0,1]:
								if 0 <= i+i2 < numPixelRows and 0 <= j+j2 < numPixelColumns:

									# Segmentation network spreads by itself, but also activate its control (inter3)
									if i2 != 0 or j2 != 0:  # or 1:
										source.append(h*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
										target.append(h*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

									########################################################################
									### THIS VERSION WORKS AS WELL!!! SEEMS MORE CLEAN AND STRUCTURED... ###
									########################################################################

									# All the segmentation magic happens in the following lines
									for k in range(numOrientations):
										if boundarySegFlowFilter[s][k][i2+boundarySegSpeeds[s]][j2+boundarySegSpeeds[s]] > 0:

											# Segmentation network triggers activity in its corresponding segmentation layer in V2
											for h2 in range(h+1):
												source2.append(h   *                                                 numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
												target2.append(h2  *numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

											# V1 trigger spreading in the segmentation network (orientation specific)
											source4.append(                                                        k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
											target4.append(    h   *                                                 numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

											# V2 (corresponding segmentation layer) triggers activity in the segmentation network
											source3.append(   (h+1)*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
											target3.append(    h   *                                                 numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))

										# if boundarySegFlowFilter[s][oppOri[k]][i2+boundarySegSpeeds[s]][j2+boundarySegSpeeds[s]] > 0:
										#
										# 	# Segmentation network triggers activity in its corresponding segmentation layer in V2
										# 	for h2 in range(h+1):
										# 		source2B.append(h   *                                                 numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
										# 		target2B.append(h2  *numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
										#
										# 	# V1 trigger spreading in the segmentation network (orientation specific)
										# 	source4B.append(                                                        k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
										# 	target4B.append(    h   *                                                 numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
										#
										# 	# V2 (corresponding segmentation layer) triggers activity in the segmentation network
										# 	source3B.append(   (h+1)*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
										# 	target3B.append(    h   *                                                 numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))



						#			# #######################################################
						#			# # THIS VERSION WORKS AS WELL!!! THE ORIGINAL ONE... ###
						#			# #######################################################
						#			#
						#			# All the segmentation magic happens in the following lines
						# 			for k in range(numOrientations):
						#
						# 				# Segmentation network triggers activity in its corresponding segmentation layer in V2
						# 				for h2 in range(h+1):
						# 					source2.append(h *                                                 numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
						# 					target2.append(h2*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
						#
						# 				# V1 trigger spreading in the segmentation network (orientation specifit)
						# 				if boundarySegFlowFilter[k][i2+1][j2+1] > 0:
						# 					source4.append(k*numPixelRows*numPixelColumns +  i    *numPixelColumns +  j    )
						# 					target4.append(h*numPixelRows*numPixelColumns + (i+i2)*numPixelColumns + (j+j2))
						#
						# # V2 (corresponding segmentation layer) triggers activity in the segmentation network
						# source3.append((h+1)*numOrientations*numPixelRows*numPixelColumns + k*numPixelRows*numPixelColumns + i*numPixelColumns + j)
						# target3.append( h   *                                                 numPixelRows*numPixelColumns + i*numPixelColumns + j)

		# Input from segmentation signals and reset from reset signals, inhibition from third interneuron
		nest.Connect( BoundarySegmentationOnSignal,                 BoundarySegmentationOn,                            'one_to_one', {'weight': connections['B_SendSignalExcite'   ]})  #      1
		nest.Connect( ResetBoundarySegmentationCell,                BoundarySegmentationOn,                            'all_to_all', {'weight': connections['B_ResetSignalInhib'   ]})
		nest.Connect( BoundarySegmentationOnInter3,                 BoundarySegmentationOn,                            'one_to_one', {'weight': connections['B_InterNeuronsInhib'  ]})  # -15000

		# Spreading, once activity is present, in the boundary segmentation layer (originally inhibited by the tonic interneurons layer) and input from V1 (proto-activation)
		nest.Connect([BoundarySegmentationOn[s] for s in source ], [BoundarySegmentationOn[t]       for t in target ], 'one_to_one', {'weight': connections['B_SegmentSpreadExcite']})  #   1000
		nest.Connect([BoundarySegmentationOn[s] for s in source ], [BoundarySegmentationOnInter3[t] for t in target ], 'one_to_one', {'weight': connections['B_ControlSpreadExcite']})  #   3000
		nest.Connect([BoundarySegmentationOn[s] for s in source2], [V2Layer23Seg[t]                 for t in target2], 'one_to_one', {'weight': connections['B_TriggerV2Inhib'     ]})  #  -5000

		# V2 and V1 trigger segmentation spread, by disinhibition (strong from V2 segmentation layers, weaker - but essential for speed - from V1, some sort of pre-triggering)
		nest.Connect([V2Layer23Seg[s]           for s in source3], [BoundarySegmentationOnInter3[t] for t in target3], 'one_to_one', {'weight': connections['B_FeedbackV2Inhib'    ]})  #?-50000?
		nest.Connect([V1Layer23[s]              for s in source4], [BoundarySegmentationOnInter3[t] for t in target4], 'one_to_one', {'weight': connections['B_FeedbackV1Inhib'    ]})  # -10000

		# # TEST!!!
		# nest.Connect([BoundarySegmentationOn[s] for s in source2B], [V2Layer23Seg[t]                 for t in target2B], 'one_to_one', {'weight': -1.0*connections['B_TriggerV2Inhib'     ]})  #  -5000
		# nest.Connect([V2Layer23Seg[s]           for s in source3B], [BoundarySegmentationOnInter3[t] for t in target3B], 'one_to_one', {'weight': -1.0*connections['B_FeedbackV2Inhib'    ]})  #?-50000?
		# nest.Connect([V1Layer23[s]              for s in source4B], [BoundarySegmentationOnInter3[t] for t in target4B], 'one_to_one', {'weight': -1.0*connections['B_FeedbackV1Inhib'    ]})  # -10000

	############################################################
	### Network is defined, now set up stimulus and simulate ###
	############################################################

	# Print the size and the number of connections of the network
	sys.stdout.write('.\nOrientations:  ' + str(numOrientations) + '  ====\  Num. neurons:   ' + str(nest.GetKernelStatus('network_size'))    +
					 ' \nSegm. layers:  ' + str(numSegLayers)    + '  ====/  Num. synapses:  ' + str(nest.GetKernelStatus('num_connections')) + '\n')

	# Compute the position of the Vernier target and create templates for template match
	vPosX, vPosY, vTemplateR, vTemplateL = findVernierAndCreateTemplates(pixels, thisConditionName, templateSize)
	trialScores = []  # Will contain the Vernier evidence, for each trial

	# Create population recorders for all chosen populations
	gifMakerList = []
	for name, pop in popToRecord.iteritems():
		if name == 'V4Bright':  # V4Bright is the layer whose output is compared to the Vernier template, so it's placed first
			gifMakerList.insert(0, gifMaker(name=name, popID=pop['ID'], dimTuple=pop['dimTuple'], orientedMap=pop['orientedMap']))
		else:
			gifMakerList.append(   gifMaker(name=name, popID=pop['ID'], dimTuple=pop['dimTuple'], orientedMap=pop['orientedMap']))

	# Goes across all trials for a condition (used to keep track to stimulus times)
	for trialCount in range(numTrials):

		# Make the current stimulus directory
		thisConditionDir = 'SimFigures/'+thisConditionName
		if not os.path.exists(thisConditionDir):
			os.makedirs(thisConditionDir)
		thisTrialDir = thisConditionDir+'/Trial'+str(trialCount)
		if not os.path.exists(thisTrialDir):
			os.makedirs(thisTrialDir)

		# Create an image of the stimulus
		plotPixels = pixels/254.0
		fileName   = thisTrialDir+'/firstStimulus.GIF'
		writeGif(fileName, [plotPixels], duration=1.0)

		# Set the timing of the simulation and stimulation
		startTime = trialCount*(stimulusTimeSteps+interTrialInterval)*stepDuration
		stopTime  = startTime + stimulusTimeSteps*stepDuration

		# Feed both LGN layers with the stimulus array (each pixel is a DC current)
		nest.SetStatus(LGNBrightInput, [{'amplitude':inputScale*inputGain*light, 'start':startTime, 'stop':stopTime} for light in numpy.reshape(numpy.maximum(pixels/127.0-1.0, 0.0), (imageNumPixelRows*imageNumPixelColumns,))])
		nest.SetStatus(LGNDarkInput  , [{'amplitude':inputScale*inputGain*dark,  'start':startTime, 'stop':stopTime} for dark  in numpy.reshape(numpy.maximum(1.0-pixels/127.0, 0.0), (imageNumPixelRows*imageNumPixelColumns,))])

		# Damping constant input, to control the amount of grouping
		nest.SetStatus(GroupDampingCell,      [{'amplitude':inputScale*dampingInput, 'start':(startTime), 'stop':(startTime + dampingDelays[k])}                  for k in range(numOrientations)])
		if len(boundaryGroupSpeeds) > 1:
			nest.SetStatus(ConstantInputLong, [{'amplitude':inputScale*longInput,    'start':(startTime), 'stop':(stopTime  + interTrialInterval*stepDuration/2)} for k in range(numOrientations)])

		# Set the segmentation signals for each non-basic segmentation layer
		if numSegLayers > 1 and useSegmentation:
			segSignalPlot      = numpy.zeros((numPixelRows, numPixelColumns))
			segSignalLocations = chooseSegmentationSignal(thisConditionName, numSegLayers)
			for h in range(numSegLayers-1):

				# Build the segmentation map, according to the input and whether CMF is used or not
				[segLocX, segLocY] = [segSignalLocations[2*h], segSignalLocations[2*h+1]]
				segMap = createSegMap(useCMF, pixels, thisConditionName, segLocX, segLocY, segSignalSize, segLocSD, segSignalAmplitude, eccentricity, pixPerDeg, a, b)
				segMap = numpy.hstack((segMap, numpy.zeros((segMap.shape[0], 1))))
				segMap = numpy.vstack((segMap, numpy.zeros((1, segMap.shape[1]))))
				segSignalPlot += segMap

				# Send the signal to the segmentation layer
				for i in range(numPixelRows):         # Rows
					for j in range(numPixelColumns):  # Columns
						A = segSignalAmplitude*segMap[i,j]
						nest.SetStatus([BoundarySegmentationOnSignal[h*numPixelRows*numPixelColumns + i*numPixelColumns + j]],
						               [{"amplitude": inputScale*A, "start": startTime + startSegSignal, "stop": startTime + startSegSignal + segSignalDuration}])

		# Simulate the network
		sys.stdout.write('\nSimulating trial '+str(trialCount)+' for condition "'+str(thisConditionName) + '"')
		for time in range(int(stimulusTimeSteps)):

			# Run the simulation for one gif frame
			nest.Simulate(stepDuration)
			if time < stimulusTimeSteps-1:
				sys.stdout.write("\033[2F") # move the cursor back to previous line

			# Take screenshots of every recorded population
			for instance in gifMakerList: # gifMaker.getInstances():
				instance.takeScreenshot()

		# Compute match with Vernier mask, for each frame (the first element of the list is the gif of V4Bright)
		templateMatches = gifMakerList[0].computeMatch(vTemplateR, vTemplateL, vSide)
		f = open(thisTrialDir+'/TemplateMatchLevels.txt', 'w')
		f.write('\n'+'Time'.rjust(6)+'\t'+str([('Seg'+str(h)).ljust(4) for h in range(numSegLayers)])+'\n\n')
		for t in range(int(stimulusTimeSteps)):
			f.write(str(t*stepDuration).rjust(6)+'\t'+str(['%.2f' % s for s in templateMatches[t]])+'\n')
		f.write('\n'+'Mean'.rjust(6)+'\t'+str(['%.2f' % s for s in numpy.mean(templateMatches, axis=0)])+'\n')
		f.close()
		trialScores.append(max(numpy.mean(templateMatches, axis=0)))

		# Create animated gif of stimulus
		sys.stdout.write('Creating animated gifs.')
		sys.stdout.flush()
		for instance in gifMakerList: # gifMaker.getInstances():
			instance.createGif(thisTrialDir, durationTime=stepDuration/1000.0*gifSlowMotionRate)

		# Inter-trial interval
		if trialCount < numTrials-1: # if that does not work, use: "if not trialCount == numTrials-1:"
			sys.stdout.write('\n\nSimulating inter-trial interval between trials ' + str(trialCount) + ' and ' + str(trialCount+1))
			nest.Simulate(interTrialInterval*stepDuration)

	# Compute model evidence (and hence crowding) for the condition
	f = open(thisConditionDir+'/AverageModelEvidence.txt', 'w')
	f.write('\n'+'Trial'.rjust(8)+'   ModelEvidence\n')
	for trial in range(len(trialScores)):
		f.write(str(trialCount).rjust(8)+'   '+'%.2f' % trialScores[trial]+'\n')
	f.write('\n'+'Mean'.rjust(8)+' = '+str(numpy.mean(trialScores))+'\n')
	f.write('Thresh'.rjust(8)+' = '+str(0.5-numpy.mean(trialScores))+'\n')
	f.write('StdDev'.rjust(8)+' = '+str(numpy.std(trialScores))+'\n')
	f.close()

###############################################
### Main loop (using multithreading or not) ###
###############################################

if __name__ == '__main__':

	# Display welcome message
	sys.stdout.write('\n\n#########################################')
	sys.stdout.write(  '\n##                                     ##')
	sys.stdout.write(  '\n## -- WELCOME TO THE LAMINART MODEL -- ##')
	sys.stdout.write(  '\n##                                     ##')
	sys.stdout.write(  '\n#########################################')

	# Run the Laminart model for all conditions
	if nCoresToUse > 1:
		pool = Pool(nCoresToUse)              # Pool(n) to use n cores ; default is max number of cores
		pool.map(runOneStim, ConditionNames)  # Process the iterable using multiple cores
	else:
		for thisConditionName in ConditionNames:
			runOneStim(thisConditionName)

	# Display goodbye message
	sys.stdout.write('\n\n\n########################################')
	sys.stdout.write(    '\n##                                    ##')
	sys.stdout.write(    '\n## -- SIMULATION FINISHED. THANKS! -- ##')
	sys.stdout.write(    '\n##                                    ##')
	sys.stdout.write(    '\n########################################\n\n\n')
