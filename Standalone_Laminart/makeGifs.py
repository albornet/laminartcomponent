# Class used to make gifs out of population's activities
import nest, numpy
from images2gif import writeGif

class gifMaker:

	def __init__(self, name, popID, dimTuple, orientedMap):

		self.name        = name                     # population name
		self.popID       = popID                    # actual population
		popSD            = nest.Create('spike_detector', numpy.prod(dimTuple))
		nest.Connect(popID, popSD, 'one_to_one')
		self.popSD       = popSD                    # population spike detector
		self.dimTuple    = dimTuple                 # population dimensions (rows, cols, etc.)
		self.plotVector  = numpy.zeros(len(popSD))  # used to plot each frame of the gif
		self.cumuVector  = numpy.zeros(len(popSD))  # used to clean the spike count from previous steps
		self.maxAllTime  = 1.0                      # used to normalize the images in the gif
		self.outImages   = [[] for i in range(dimTuple[0])]  # list of images that will build the gif
		self.orientedMap = orientedMap              # whether the gif takes colours into account
		self.takeMax     = True                     # whether only the maximally active orientation is plotted

	def takeScreenshot(self):

		# Record the spikes of the current step and update the cumulative vector (to substract previous spikes)
		self.plotVector  = nest.GetStatus(self.popSD, 'n_events') - self.cumuVector
		self.cumuVector += self.plotVector

		# Take care of output normalization
		maxThisTime      = numpy.max(self.plotVector)
		if 1: # maxThisTime > 0:
			self.maxAllTime = max(maxThisTime, self.maxAllTime)

			# Oriented output screenshot
			if self.orientedMap:

				(nSeg, nOri, nRow, nCol) = self.dimTuple
				thisPlot = numpy.reshape(self.plotVector, self.dimTuple)

				for h in range(nSeg):

					if self.takeMax:
						for i in range(nRow):
							for j in range(nCol):
								oriToSave = numpy.argmax(thisPlot[h,:,i,j])
								oriToKill = [x for x in range(nOri) if x != oriToSave]
								thisPlot[h,oriToKill,i,j] = 0.0

					if nOri == 2:
						self.outImages[h].append(numpy.dstack((thisPlot[h,0,:,:], thisPlot[h,1,:,:], numpy.zeros((nRow,nCol)))))
					if nOri == 4:
						self.outImages[h].append(numpy.dstack((thisPlot[h,1,:,:], thisPlot[h,3,:,:], numpy.maximum(thisPlot[h,0,:,:], thisPlot[h,2,:,:]))))
					if nOri == 8:
						rgbMap = numpy.array([[0.,.5,.5], [0.,0.,1.], [.5,0.,.5], [1.,0.,0.], [.5,0.,.5], [0.,0.,1.], [0.,.5,.5], [0.,1.,0.]])

						useNew = 1
						if useNew:
							self.outImages[h].append(numpy.tensordot(thisPlot[h,:,:,:], rgbMap, axes=(0,0)))

						# For now nOri==8 does not produce the correct gif, so I let the previous version
						else:
							data = numpy.zeros((nRow,nCol,3), dtype=numpy.uint8)
							for i in range(nRow):
								for j in range(nCol):
									temp = []
									for k in range(nOri):
										temp.append(thisPlot[h][k][i][j])
									data[i][j] = rgbMap[numpy.argmax(temp)]*numpy.max(temp)
							self.outImages[h].append(data)

			# Simple output screenshot
			else:
				(nSeg, nRow, nCol) = self.dimTuple
				for h in range(nSeg):
					self.outImages[h].append(numpy.reshape(self.plotVector, self.dimTuple)[h,:,:])

	def createGif(self, thisTrialDir, durationTime):

		# Create an animated gif of the output ; rescale firing rates to max value
		if self.dimTuple[0] > 1:
			for h in range(self.dimTuple[0]):
				writeGif(thisTrialDir+'/'+self.name+'Seg'+str(h)+'.gif', numpy.array(255*self.outImages[h])/self.maxAllTime, duration=durationTime)
		else:
			writeGif(thisTrialDir+'/'+self.name+'.gif', numpy.array(255*self.outImages[0])/self.maxAllTime, duration=durationTime)

		# Reset the output images, for the next trials
		self.outImages = [[] for i in range(self.dimTuple[0])]

	def computeMatch(self, vernierTemplateRight, vernierTemplateLeft, vSide):

		templateScores = [[] for h in range(self.dimTuple[0])]
		for h in range(self.dimTuple[0]):
			for frame in self.outImages[h]:
				tempR = numpy.sum(frame*vernierTemplateRight)
				tempL = numpy.sum(frame*vernierTemplateLeft)
				templateScores[h].append(vSide*(tempR - tempL)/(100.0 + tempR + tempL))

		return numpy.transpose(templateScores)
