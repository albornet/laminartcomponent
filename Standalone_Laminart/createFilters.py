# Imports
import numpy, matplotlib.pyplot as plt


# Function 1: Take filter parameters and build 2 oriented filters with different polarities for connection pattern from the LGN to V1
def createFilters(nOri, size, phi, sigmaX, sigmaY, oLambda, plot=False):

    # # Initialize the filters
    # filters = numpy.zeros((nOri, size, size))
    #
    # # Fill them with gabors
    # midSize = (size-1.)/2.
    # maxValue = -1
    # for k in range(0, nOri):
    #     theta = numpy.pi*(k+1)/nOri + phi
    #     for i in range(0, size):
    #         for j in range(0, size):
    #             x = (i-midSize)*numpy.cos(theta) + (j-midSize)*numpy.sin(theta)
    #             y = -(i-midSize)*numpy.sin(theta) + (j-midSize)*numpy.cos(theta)
    #             filters[k][i][j] = numpy.exp(-((x*x)/sigmaX + (y*y)/sigmaY)) * numpy.sin(2*numpy.pi*x/oLambda)
    #
    # # Normalize (and tune if necessary) the orientation filters
    # for k in range(nOri):
    #     sumXX = numpy.sum(filters[k]*filters[k])
    #     sumXX = numpy.sqrt(sumXX)
    #     if nOri == 8 and k in [3,7]:
    #         sumXX *= 1.00  # 1.00
    #     if nOri == 8 and k in [1,5]:
    #         sumXX *= 1.25  # 1.25 - 1.30
    #     if nOri == 8 and k in [0,2,4,6]:
    #         sumXX *= 0.75  # 0.66
    #     filters[k] /= 1.0*sumXX

    filters = numpy.array([
               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0,-0.5,-0.2,-0.1, 0.0],
                [0.0, 0.1, 0.2, 0.5, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.0, 0.0, 0.0],
                [0.0,-0.0, 0.0, 0.5, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.1, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.2, 0.0, 0.0],
                [0.0, 0.0,-0.2, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.1, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.5, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0,-0.1, 0.0, 0.0, 0.0],
                [0.0, 0.0,-0.2, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.2, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.1, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0,-0.0, 0.0, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.5, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.1, 0.2, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.5,-0.2,-0.1, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],

               [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.5, 0.5, 0.0, 0.0],
                [0.0, 0.0,-0.5,-0.5, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]])

    # import cv2
    # filters = []
    # for k in range(nOri):
    #     theta = numpy.pi/2 - (numpy.pi*(k+1)/nOri + phi)
    #     params = {'ksize': (size, size), 'sigma': 0.4, 'theta': theta, 'lambd': 4, 'gamma': 0.3}
    #     thisFilter = numpy.array(cv2.getGaborKernel(**params))
    #     if size%2 == 0:
    #         thisFilter = numpy.delete(thisFilter, size/2, 0)
    #         thisFilter = numpy.delete(thisFilter, size/2, 1)
    #     if k == 1:
    #         thisFilter[+1:size/2+1, size/2-1:-1] = thisFilter[:size/2, size/2:]
    #         thisFilter[size/2-1:-1, +1:size/2+1] = thisFilter[size/2:, :size/2]
    #     if k == 5:
    #         thisFilter[+1:size/2+1, +1:size/2+1] = thisFilter[:size/2, :size/2]
    #         thisFilter[size/2-1:-1, size/2-1:-1] = thisFilter[size/2:, size/2:]
    #     filters.append(thisFilter)
    # filters = numpy.array(filters)

    # Normalize (and tune if necessary) the orientation filters
    for k in range(nOri):
        sumXX = numpy.sqrt(numpy.sum(filters[k]**2))
        if nOri == 8 and k in [3,7]:
            sumXX *= 1.00  # 1.00
        if nOri == 8 and k in [1,5]:
            sumXX *= 1.10  # 1.30
        if nOri == 8 and k in [0,2,4,6]:
            sumXX *= 0.80  # between 0.85 and 0.87
        filters[k] /= sumXX

    # # Supress small values and gain the filters
    # filters[numpy.abs(filters) < 0.1] = 0.0

    if 0 and plot:
        for k in range(nOri):
            plt.figure()
            plt.imshow(filters[k], interpolation='nearest')
            plt.show()

    return filters, -filters


# Function 2: Take filter parameters and build connection pooling and connection filters arrays
def createPoolingConnectionsAndFilters(numOrientations, VPoolSize, phi):

    # Build the angles in radians, based on the taxi-cab distance (useful for 8 orientations)
    angles = []
    for k in range(numOrientations/2):
        taxiCabDistance = 4.0*(k+1)/numOrientations
        try:
            alpha = numpy.arctan(taxiCabDistance/(2 - taxiCabDistance))
        except ZeroDivisionError:
            alpha = numpy.pi/2 # because tan(pi/2) = inf
        angles.append(alpha + numpy.pi/4)
        angles.append(alpha - numpy.pi/4)

    # This is kind of a mess, but it works
    for k in range(len(angles)):
        if angles[k] <= 0.0:
            angles[k] += numpy.pi
        if numOrientations == 2: # special case ... but I could not do it otherwise
            angles[k] += numpy.pi/4

    # Sort the angles, because they are generated in a twisted way (hard to explain, but we can see that on skype)
    angles = numpy.sort(angles)

    # Set up orientation kernels for each filter
    midSize = (VPoolSize-1.0)/2.0
    VPoolingFilters = numpy.zeros((numOrientations, VPoolSize, VPoolSize))
    for k in range(0, numOrientations):
        theta = angles[k] + phi
        for i in range(0, VPoolSize):
            for j in range(0, VPoolSize):

                # Transformed coordinates: rotation by an angle of theta
                x =  (i-midSize)*numpy.cos(theta) + (j-midSize)*numpy.sin(theta)
                y = -(i-midSize)*numpy.sin(theta) + (j-midSize)*numpy.cos(theta)

                # If the rotated x value is zero, that means the pixel(i,j) is exactly at the right angle
                if numpy.abs(x) < 0.001:
                    VPoolingFilters[k][i][j] = 1.0

    # Set layer23 pooling connections (connect to points at either extreme of pooling line ; 1 = to the right ; 2 = to the left)
    VPoolingConnections1 = VPoolingFilters.copy()
    VPoolingConnections2 = VPoolingFilters.copy()

    # Do the pooling connections
    for k in range(numOrientations):

        # Want only the end points of each filter line (remove all interior points)
        for i in range(1, VPoolSize - 1):
            for j in range(1, VPoolSize - 1):
                VPoolingConnections1[k][i][j] = 0.0
                VPoolingConnections2[k][i][j] = 0.0

        # Segregates between right and left directions
        for i in range(VPoolSize):
            for j in range(VPoolSize):
                if j == (VPoolSize-1)/2:
                    VPoolingConnections1[k][0][j] = 0.0
                    VPoolingConnections2[k][VPoolSize-1][j] = 0.0
                elif j < (VPoolSize-1)/2:
                    VPoolingConnections1[k][i][j] = 0.0
                else:
                    VPoolingConnections2[k][i][j] = 0.0

    # Returns all the needed filters to the program
    return VPoolingFilters, VPoolingConnections1, VPoolingConnections2

# Set up filters for Brightness/Darkness/SurfaceSeg/BoundarySeg spreading and boundary blocking
def createBrightnessFilters(numOrientations, numBrightnessFlows, brightnessSpreadingSpeeds):

    # Set up filters for Brightness/Darkness filling-in stage (spreads in various directions) and boundary blocking
    brightnessFlowFilter = []
    H = numOrientations   - 1  # Vertical orientation index
    V = numOrientations/2 - 1  # Horizontal orientation index
    notStopFlowOrientation = [V, H]
    if numBrightnessFlows == 8:
        UpRight   = 1
        DownRight = 5
        notStopFlowOrientation = [V, H, UpRight, DownRight]

    brightnessBoundaryBlockFilter = []
    for k in range(len(brightnessSpreadingSpeeds)):
        if numBrightnessFlows == 4:      # Right, Down, Left, Up
            brightnessFlowFilter.append([[ brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                             brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                            -brightnessSpreadingSpeeds[k]]])
        if numBrightnessFlows == 8:      # Up, Right, Up, Left, UpRight, UpLeft, DownRight, DownLeft
            brightnessFlowFilter.append([[ brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                             brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  0                           ],
                                         [ 0,                            -brightnessSpreadingSpeeds[k]],
                                         [ brightnessSpreadingSpeeds[k], -brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k], -brightnessSpreadingSpeeds[k]],
                                         [ brightnessSpreadingSpeeds[k],  brightnessSpreadingSpeeds[k]],
                                         [-brightnessSpreadingSpeeds[k],  brightnessSpreadingSpeeds[k]]])
        brightnessBoundaryBlockFilter.append([])
        directionBBF1 = []
        directionBBF2 = []
        directionBBF3 = []
        directionBBF4 = []

        # One direction
        for d in range(1, (
            brightnessSpreadingSpeeds[k]+1)):  # First index indicates the only orientation that does NOT block flow
            directionBBF1.append([notStopFlowOrientation[0],  -d,      0   ])  # Up
            directionBBF1.append([notStopFlowOrientation[0],  -d,     -1   ])  # Up
            directionBBF2.append([notStopFlowOrientation[1],  -1,     -d   ])  # Right
            directionBBF2.append([notStopFlowOrientation[1],   0,     -d   ])  # Right
            directionBBF3.append([notStopFlowOrientation[0], ( d-1),   0   ])  # Up
            directionBBF3.append([notStopFlowOrientation[0], ( d-1),  -1   ])  # Up
            directionBBF4.append([notStopFlowOrientation[1],  -1,    ( d-1)])  # Left
            directionBBF4.append([notStopFlowOrientation[1],   0,    ( d-1)])  # Left
        brightnessBoundaryBlockFilter[k].append(directionBBF1)
        brightnessBoundaryBlockFilter[k].append(directionBBF2)
        brightnessBoundaryBlockFilter[k].append(directionBBF3)
        brightnessBoundaryBlockFilter[k].append(directionBBF4)

        if numBrightnessFlows == 8:  # Includes main diagonals
            directionBBF5 = []
            directionBBF6 = []
            directionBBF7 = []
            directionBBF8 = []
            for d in range(1,(brightnessSpreadingSpeeds[k]+1)):                    # First index indicates the only orientation that does NOT block flow
                directionBBF5.append([notStopFlowOrientation[2],  -d,     -d   ])  # UpRight
                directionBBF6.append([notStopFlowOrientation[3], ( d-1),  -d   ])  # UpLeft
                directionBBF7.append([notStopFlowOrientation[3],  -d,    ( d-1)])  # DownRight
                directionBBF8.append([notStopFlowOrientation[2], ( d-1), ( d-1)])  # DownLeft
            brightnessBoundaryBlockFilter[k].append(directionBBF5)
            brightnessBoundaryBlockFilter[k].append(directionBBF6)
            brightnessBoundaryBlockFilter[k].append(directionBBF7)
            brightnessBoundaryBlockFilter[k].append(directionBBF8)

    return brightnessFlowFilter, brightnessBoundaryBlockFilter


# Set up filters for boundary grouping spreads (illusory contours)
def createGroupingFilters(numOrientations, boundaryGroupingSpeeds, phi):

     # Set up filters for Boundary grouping (nearest neighbors); there are 2 filters, one for grouping in one direction and another for the other direction
    boundaryGroupFilter = [[] for s in range(2)]
    for side in range(2):                             # Grouping either or one side or on the other side
        sideValue = int((-2)*(side-0.5))              # Useful sign (1 for side = 0, -1 for side = 1)
        for h in range(len(boundaryGroupingSpeeds)):  # Grouping at different ranges

            # Identify center of filter
            center_xy = boundaryGroupingSpeeds[h]
            boundaryGroupFilter[side].append(numpy.zeros((numOrientations, numOrientations, 2*center_xy+1, 2*center_xy+1)))
            for k in range(numOrientations):          # Source orientation

                thetaSource = numpy.pi*(k+1)/numOrientations + phi
                for k2 in range(numOrientations):     # Target orientation

                    thetaTarget      = numpy.pi*(k2+1)/numOrientations + phi          # Originally numpy.pi*(k2)/numOrientations
                    orientationMatch = numpy.abs(numpy.cos(thetaSource-thetaTarget))  # Match of source and target orientations
                    for i in range(2*boundaryGroupingSpeeds[h]+1):
                        for j in range(2*boundaryGroupingSpeeds[h]+1):

                            thetaPixel = numpy.arctan2(-(i-center_xy), (j-center_xy))           # /!\ "-i" over "j" (trigonometry: "-y" over "x")
                            orientationMatch2 = numpy.abs( numpy.cos( thetaSource-thetaPixel))  # source orientation vs neighbor position
                            orientationMatch3 = numpy.abs( numpy.cos( thetaTarget-thetaPixel))  # target orientation vs neighbor position
                            orientationMatch4 = numpy.sin(-numpy.pi/2+thetaTarget-thetaPixel )  # target orientation vs neighbor position, to make directional filter
                            value = orientationMatch * orientationMatch2 * (orientationMatch3**3) * orientationMatch4

                            # Insert the value in the corresponding grouping filter
                            if sideValue*value >= 1.0 and not (i==center_xy and j==center_xy):
                                distanceToCenter = numpy.sqrt((i-center_xy)**2 + (j-center_xy)**2)
                                boundaryGroupFilter[side][h][k][k2][i][j] = sideValue*value**3/distanceToCenter

                    # print (thetaSource, thetaTarget)
                    # for i in range(2*boundaryGroupingSpeeds[h]+1):
                    #     print ['%.1f' % boundaryGroupFilter[side][h][k][k2][i][j] for j in range(2*boundaryGroupingSpeeds[h]+1)]
                    # print

    return boundaryGroupFilter


# Set up filters for segmentation spreading (use same architecture as grouping filters)
def createSegmentationFilters(numOrientations, boundarySegmentationSpeeds, phi):

    # The boundary segmentation flow filter is simply the smallest grouping flow filter, with no restriction for the side of flow and the target orientation
    boundarySegFlowFilters = []
    for s in boundarySegmentationSpeeds:
        basisFilters = createGroupingFilters(numOrientations, [s], phi)
        boundarySegFlowFilter = [basisFilters[0][0][ori][ori][:][:] + basisFilters[1][0][ori][ori][:][:] for ori in range(numOrientations)]
        for k in range(numOrientations):
            boundarySegFlowFilter[k][1][1] = 1.0
        if numOrientations == 8 and s == 1:
            for k in range(numOrientations):
                if k in [0,2,4,6]:
                    boundarySegFlowFilter[k] = 0.5*(boundarySegFlowFilter[k-1] + boundarySegFlowFilter[k+1])
        boundarySegFlowFilters.append(boundarySegFlowFilter)

    return boundarySegFlowFilters


# Define illusory contours spreading delays, according to the general orientation of the stimulus
def generateDelays(thisConditionName, pixels, numOrientations):

    # from scipy import signal

    # giantSize = max(pixels.shape[0],pixels.shape[1])
    # giantFilters1, giantFilters2 = createFilters(numOrientations, size=giantSize, phi=0, sigmaX=0.2*giantSize, sigmaY=20*giantSize, oLambda=giantSize, plot=False)
    # giantFilters = numpy.abs(giantFilters1)

    # firstRow = int((giantSize-pixels.shape[0])/2)
    # firstCol = int((giantSize-pixels.shape[1])/2)

    # match = numpy.zeros((numOrientations,))
    # for k in range(numOrientations):
    #     convResult = signal.convolve2d(giantFilters[k], pixels, boundary='symm', mode='same')
    #     match[k]   = numpy.max(convResult) + numpy.mean(convResult)

    # gain  = 100000.0*((pixels.shape[0]*pixels.shape[1])/(numpy.sum(pixels)))**2
    # match = gain*(numpy.arctan((match-3000.0)/200.0)+numpy.pi/2)
    # match[match != numpy.max(match)] = 1.0

    # for k in range(numOrientations):
    #     theta = 180.0*(k+1)/numOrientations
    #     print (theta, match[k])

    # Gives back the delays (or not)
    # return match

    # For now, the upper part of the code is not used, so the part below returns the delays
    HD1 = 0.0
    D1  = 0.0
    D1V = 0.0
    V   = 0.0
    VD2 = 0.0
    D2  = 0.0
    D2H = 0.0
    H   = 0.0
    if thisConditionName in ['text 1', 'text 2']:
        H = 20.0
    if thisConditionName in ['malania short 4', 'malania short 8', 'squares 2']:
        H = 30.0 # 30.0
    if thisConditionName in ['octagons grouping', 'octagons 3', 'circles 6', 'squares 4']:
        H = 50.0 # 50.0

    if numOrientations == 2:
        dampingDelays = [V, H]
    if numOrientations == 4:
        dampingDelays = [D1, V, D2, H]
    if numOrientations == 8:
        dampingDelays = [HD1, D1, D1V, V, VD2, D2, D2H, H]

    return dampingDelays
