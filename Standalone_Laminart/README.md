# README #

LAMINART model

### What is it for? ###

* This is a spiking cortical model for early-stage visual segmentation.

### How do I get set up? ###

* Set up steps:
	- cd <directoryOfYourChoice>
	- git clone https://<username>@bitbucket.org/albornet/laminart.git
	- cd laminart
	- unzip Stimuli.zip
	- mv Stimuli ..
* Dependencies: nest, numpy
* You can edit the models parameters at the beginning of Laminart.py
* How to run: open a terminal in the Laminart folder and run 'python Laminart.py'
* Were to find results: activity gifs for various layers are generated in /SimFigure

### Who do I talk to? ###

* Repository owner: alban.bornet@epfl.ch
